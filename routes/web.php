<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HomeMessageController;
use App\Http\Controllers\HomeAssetsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DivisionController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\ItadminController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\WebinfoController;
use App\Http\Controllers\FacilityController;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

// namespace App\Http\Middleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'index'])->middleware('guest');
Route::post('/', [HomeController::class, 'ticket'])->middleware('guest');
Route::post('/message', [HomeMessageController::class, 'message'])->middleware('guest');
// Route::get('/', [HomeController::class, 'store'])->middleware('guest');

//auth
Route::get('/login', [AuthController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [AuthController::class, 'authenticate']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);

//it administration
// Route::get('it', function(){
//     return view('administration');
// });
Route::get('/administration', [AdminController::class, 'index'])->middleware('guest');
Route::get('/borrow', [HomeController::class, 'borrow'])->middleware('guest');
Route::post('/borrow', [HomeAssetsController::class, 'borrow'])->middleware('guest');


//admin
Route::get('/admin', function(){
    return view('admin.index');
})->middleware('auth');


// Route::get('/admin/karyawan/checkSlug', [AdminUserController::class, 'checkSlug'])->middleware('auth');
Route::resource('/admin/karyawan', AdminUserController::class)->middleware('auth');
Route::resource('/admin/item', ItemController::class)->middleware('auth');
Route::resource('/admin/category', CategoryController::class)->middleware('auth');
Route::resource('/admin/division', DivisionController::class)->middleware('auth');
Route::resource('/admin/ticket', TicketController::class);
Route::resource('/admin/itadmin', ItadminController::class)->middleware('auth');
//it administration download
// Route::get('/forDownload', [ItadminController::class, 'forDownload']);
Route::get('/forDownload/{id}', [ItadminController::class, 'forDownload']);

Route::resource('/admin/contact', ContactController::class)->middleware('auth');
Route::resource('/admin/webinfo', WebinfoController::class)->middleware('auth');
Route::resource('/admin/facility', FacilityController::class)->middleware('auth');



