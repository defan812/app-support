@extends('layouts.main')

@section('container')
    
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container position-relative">
      <h1>Welcome to App Support</h1>
      <h2>We are a support team that handles it inventory and IT Administration</h2>
      @if(session()->has('success'))
        <h3 class="">{{ session('success') }}</h3>
      @endif
      <a href="#cta" class="btn-get-started scrollto">Is There Any Problem?</a>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg">
      <div class="container">

        <div class="row d-flex align-items-center justify-content-center ">

          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="assets/img/clients/cls.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Services</h2>
          <p>Provides several services such as Employees, IT inventory and IT administration. The employee feature manages registration and application access rights, 
            the IT inventory feature manages IT assets including borrowing and procurement in the IT division, the IT Administration feature manages asset documents from borrowing, procurement and payments in the IT division. </p>
        </div>

        <div class="row">

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box iconbox-blue">
              <div class="icon">
                <i class='bx bxs-group' ></i>
              </div>
              <h4><a href="">Employees</a></h4>
              <p>The employee feature manages registration and application access rights</p>
              <div id="" class="my-3">
                <a href="/register" class="btn rounded-pill btn-outline-primary">Register now</a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box iconbox-orange ">
              <div class="icon">
                <i class='bx bxs-box'></i>
              </div>
              <h4><a href="">Inventory</a></h4>
              <p>the IT inventory feature manages IT assets including borrowing and procurement in the IT division</p>
              <div id="" class="my-3">
                <a href="/borrow" class="btn rounded-pill btn-outline-primary">Borrow Assets</a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box iconbox-pink">
              <div class="icon">
                <i class='bx bx-fingerprint'></i>
              </div>
              <h4><a href="">IT Administration</a></h4>
              <p>the IT Administration feature manages asset documents from borrowing, procurement and payments in the IT division</p>
              <div id="" class="my-3">
                <a href="/administration" class="btn rounded-pill btn-outline-primary">Doc Template</a>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="text-center">
          <h3>Call To Action</h3>
          <p> If there are problems with your work, you can contact pt cls system support via an open ticket using the button below</p>
          {{-- <a class="cta-btn" href="https://wa.me/6281375142009" target="_blank">Whatapps Support</a> --}}
          <a class="cta-btn" target="_blank" data-toggle="modal" data-target="#myModal">Open Ticket</a>
        </div>

      </div>

      <!-- sample modal content -->
      <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Open Ticket</h4>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                </div>
                <form action="/" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        {{-- isi content input --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="name" class="form-text text-muted">No Ticket</small>
                                    <input class="form-control @error('no_ticket') is-invalid @enderror" name="no_ticket" id="no_ticket" type="text" placeholder="No Ticket" value="{{ '#CLS-' . $request }}" readonly>
                                    @error('no_ticket')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div>                                        
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="name" class="form-text text-muted">Name</small>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                                    @error('name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div>                                        
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="name" class="form-text text-muted">Whatapps</small>
                                    <input class="form-control @error('whatapps') is-invalid @enderror" name="whatapps" id="whatapps" type="text" placeholder="Whatapps" value="{{ old('whatapps') }}" required>
                                    @error('whatapps')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div>                                        
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="name" class="form-text text-muted">Complaint</small>
                                    <input class="form-control @error('problem') is-invalid @enderror" name="problem" id="problem" type="text" placeholder="Problem" value="{{ old('problem') }}" required>
                                    @error('problem')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div>                                       
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="textHelp" class="form-text text-muted">Description</small>
                                    <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" placeholder="Text Here..." value="{{ old('description') }}" required></textarea>
                                    @error('description')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div>                                        
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <small id="textHelp" class="form-text text-muted">Image</small>
                                    <input type="file" class="form-control-file  @error('image') is-invalid @enderror" name="image" id="image" >
                                        @error('image')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>  
                                        @enderror
                                </div>
                            </div> 
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="status" id="status" placeholder="" value="Pending">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                            data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    {{-- <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
          <h2>Portfolio</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 1</h4>
              <p>App</p>
              <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 2</h4>
              <p>App</p>
              <a href="assets/img/portfolio/portfolio-3.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 2</h4>
              <p>Card</p>
              <a href="assets/img/portfolio/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 2</h4>
              <p>Web</p>
              <a href="assets/img/portfolio/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 3</h4>
              <p>App</p>
              <a href="assets/img/portfolio/portfolio-6.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 1</h4>
              <p>Card</p>
              <a href="assets/img/portfolio/portfolio-7.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 3</h4>
              <p>Card</p>
              <a href="assets/img/portfolio/portfolio-8.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="assets/img/portfolio/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox details-link" title="Portfolio Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section --> --}}

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Team</h2>
          <p>Friends who join here, those who build this application. so this application continues to grow in the hands of the team here. Introduce our team</p>
        </div>

        <div class="row d-flex align-items-center justify-content-center">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/sofyan.jpg" class="img-fluid" alt="">
                <div class="social">
                  {{-- <a href=""><i class="icofont-twitter"></i></a> --}}
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Sofyan Ardi Wibowo</h4>
                <span>Support And Devops</span>
              </div>
            </div>
          </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Pricing Section ======= -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Contact</h2>
          <p>To contact us can be via email, whatsapp, contact form or go to the address below. To start collaborating with our team.</p>
        </div>

        <div class="row">

          <div class="col-lg-6">

            <div class="row">
              <div class="col-md-12">
                <div class="info-box">
                  <i class="bx bx-map"></i>
                  <h3>Our Address</h3>
                  <p>Jl. Kebon Jeruk VII No.2E, Maphar, Taman Sari, Jakarta Barat, 11160</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4">
                  <i class="bx bx-envelope"></i>
                  <h3>Email Us</h3>
                  <p>info1@cls-indo.com<br>itsupport@cls-indo.com</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4">
                  <i class="bx bx-phone-call"></i>
                  <h3>Call Us</h3>
                  <a class="btn btn-outline-warning rounded-pill" href="https://wa.me/6281375142009" target="_blank">Whatapps +6281375142009</a>
                  {{-- <p>+1 5589 55488 55<br>+1 6678 254445 41</p> --}}
                </div>
              </div>
            </div>

          </div>

          <div class="col-lg-6">
            <form action="/message" method="post">
              @csrf
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="names" class="form-control @error('names') is-invalid @enderror" id="name" placeholder="Your Name" required/>
                  @error('names')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>  
                  @enderror
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Your Email" required/>
                  @error('email')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>  
                  @enderror
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" id="subject" placeholder="Subject" required/>
                  @error('subject')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>  
                  @enderror
              </div>
              <div class="form-group">
                <textarea class="form-control @error('message') is-invalid @enderror" name="message" rows="5" data-rule="required" placeholder="Message" required></textarea>
                @error('message')
                    <div class="invalid-feedback">
                      {{ $message }}
                    </div>  
                  @enderror
              </div>
              <div class="text-center"><button class="btn btn-outline-warning rounded-pill" type="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

 @endsection