@extends('auth.main')    

@section('container')
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative"
            style="background:url(/dashboard/assets/images/big/auth-bg.jpg) no-repeat center center;">
            <div class="auth-box row text-center">
                {{-- <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url(/dashboard/assets/images/big/3.jpg);"> --}}
                </div>
                <div class="col-lg-5 col-md-7 my-5 bg-white">
                    <div class="p-3">
                        {{-- <img src="/dashboard/assets/images/big/icon.png" alt="wrapkit"> --}}
                        <h2 class="mt-3 text-center">Borrow Assets</h2>
                        <hr>
                        {{-- <small>Will be sent by email to hrd , use to register workers in that email app-support PT CLS System</small> --}}
                        <form action="/borrow" method="post" class="mt-4">
                            @csrf
                            
                            <div class="col-lg-12">
                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Name</small>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                                            @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <small id="no_identity" class="form-text text-muted">No iIdentity</small>
                                            <input class="form-control @error('no_identity') is-invalid @enderror" name="no_identity" id="no_identity" type="text" placeholder="No Identity" value="{{ old('no_identity') }}" required>
                                            @error('no_identity')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>

                                </div>  
                            </div> 
                            <div class="col-lg-12">
                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <small id="division" class="form-text text-muted">Division</small>
                                            <select class="form-control @error('division') is-invalid @enderror" name="division" id="division" value="{{ old('division') }}" required>
                                                <option value="">Select One</option>
                                                @foreach ($division as $division)  
                                                    <option value="{{ $division->division }}">{{ $division->division }}</option>
                                                @endforeach
                                            </select>
                                            @error('division')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <small id="position" class="form-text text-muted">Position</small>
                                            <input class="form-control @error('position') is-invalid @enderror" name="position" id="position" type="text" placeholder="Position" value="{{ old('position') }}" required>
                                            @error('position')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>

                                </div>        
                            </div> 

                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <small id="whatsapp" class="form-text text-muted">Whatsapp</small>
                                            <input class="form-control @error('whatsapp') is-invalid @enderror" name="whatsapp" id="whatsapp" type="text" placeholder="Whatsapp" value="{{ old('whatsapp') }}" required>
                                            @error('whatsapp')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <small id="status" class="form-text text-muted">Status</small>

                                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" type="text" placeholder="Status" value="{{ old('status') }}">
                                                <option value="">Select One</option>
                                                <option value="Borrow">Borrow</option>
                                                <option value="Return">Return</option>
                                            </select>
                                            @error('status')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div> 
                             

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="address" class="form-text text-muted">Address</small>
                                    <input class="form-control @error('address') is-invalid @enderror" name="address" id="address" type="text" placeholder="Address" value="{{ old('address') }}" required>
                                    @error('address')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div> 
                            
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <small id="note" class="form-text text-muted">note</small>
                                    <input class="form-control @error('note') is-invalid @enderror" name="note" id="note" type="text" placeholder="note" value="{{ old('note') }}" required>
                                    @error('note')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>  
                                    @enderror
                                </div>
                            </div> 
                            <div class="col-lg-12 d-flex justify-content-center">
                                <div id="dynamicAddRemove">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                {{-- <input name="addMore[0][id_facility]" id="addMore[][id_facility]" type="hidden" value="ambil dari id table facilities"> --}}
                                                <div class="form-group">
                                                    <small id="name_item" class="form-text text-muted">Item Name</small>
                                                    {{-- <select class="form-control @error('name_item') is-invalid @enderror" name="name_item[]" id="addMore[][name_item]" value="{{ old('name_item') }}" >
                                                        <option value="">Select One</option>
                                                        @foreach ($item as $item)  
                                                            <option value="{{ $item->product_name }}">{{ $item->product_name }}</option>
                                                        @endforeach
                                                    </select> --}}
                                                    <input class="form-control @error('name_item') is-invalid @enderror" name="name_item[]" id="addMore[][name_item]" type="text" placeholder="Name Item" value="{{ old('name_item') }}">
                                                    @error('name_item')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <small id="brand" class="form-text text-muted">Brand</small>
                                                    <input class="form-control @error('brand') is-invalid @enderror" name="brand[]" id="addMore[][brand]" type="text" placeholder="Brand" value="{{ old('brand') }}">
                                                    @error('brand')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div> 
                                            
                                            <div class="col-lg-1">

                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <small id="qty" class="form-text text-muted">Qty</small>
                                                    <input class="form-control @error('qty') is-invalid @enderror" name="qty[]" id="addMore[][qty]" type="text" placeholder="Qty" value="{{ old('qty') }}" >
                                                    @error('qty')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-lg-7">
                                                <div class="form-group">
                                                    <small id="sn" class="form-text text-muted">Serial Number</small>
                                                    <input class="form-control @error('sn') is-invalid @enderror" name="sn[]" id="addMore[0][sn]" type="text" placeholder="Serial Number" value="{{ old('sn') }}" >
                                                    @error('sn')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div> 
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="form-group">
                                                    <button type="button" name="add" id="dynamic-ar" class="btn btn-rounded btn-sm btn-outline-info mt-3">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                            <hr>
                            <div class="col-lg-12 text-center">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-block btn-dark">Submit</button>
                                    </div>
                                    <div class="col-lg-6">
                                        <a href="/" class="btn btn-block btn-danger">Back to Home</a>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="col-lg-12 text-center mt-3" >
                                Already have an account? <a href="/login" class="text-danger">Sign In</a>
                            </div> --}}

                            {{-- <div class="col-lg-12 text-center mb-3">
                                <a href="/" class="text-danger">Back to Home</a>
                            </div> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <script type="text/javascript">
            var i = 0;
            $("#dynamic-ar").click(function () {
                ++i;
                $("#dynamicAddRemove").append('\
                <div id="form1">\
                    <div class="col-lg-12">\
                        <div class="row">\
                            <div class="col-lg-6">\
                                <div class="form-group">\
                                    <small id="sn" class="form-text text-muted">Item Name</small>\
                                    <select class="form-control" name="name_item[]" id="addMore[][name_item]" value="{{ old('name_item') }}" >\
                                        <option value="">Select One</option>\
                                    </select>\
                                    <input type="text" name="name_item[]" placeholder="Item Name" class="form-control" />\
                                </div>\
                            </div>\
                            <div class="col-lg-5">\
                                <div class="form-group">\
                                    <small id="sn" class="form-text text-muted">Brand</small>\
                                    <input type="text" name="brand[]" placeholder="Brand" class="form-control" />\
                                </div>\
                            </div>\
                            <div class="col-lg-1">\
                                <div class="form-group">\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class="col-lg-12">\
                        <div class="row">\
                            <div class="col-lg-4">\
                                <div class="form-group">\
                                    <small id="sn" class="form-text text-muted">Qty</small>\
                                    <input type="text" name="qty[]" placeholder="Qty" class="form-control" />\
                                </div>\
                            </div>\
                            <div class="col-lg-7">\
                                <div class="form-group">\
                                    <small id="sn" class="form-text text-muted">Serial Number</small>\
                                    <input type="text" name="sn[]" placeholder="Serial Sumber" class="form-control" />\
                                </div>\
                            </div>\
                            <div class="col-lg-1">\
                                <div class="form-group">\
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-danger remove-input-field">Del</button>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>'
                );
            });
            $(document).on('click', '.remove-input-field', function () {
                $(this).parents('#form1').remove();
            });
        </script>
@endsection