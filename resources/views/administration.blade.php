@extends('auth.main')    

@section('container')
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative"
            style="background:url(/dashboard/assets/images/big/auth-bg.jpg) no-repeat center center;">
            
                <!-- *************************************************************** -->
                <!-- Start Sales Charts Section -->
                <!-- *************************************************************** -->
                <div class="row my-5 mx-5">
                  @foreach ($itadmin as $it)
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <div class="card-body text-center">
                                  <div class="icon-box">
                                    <div class="icon">
                                      <img class="mb-3" src="/dashboard/assets/images/big/file-text.svg" alt="" width="20%">
                                    </div>
                                    {{-- <h4 class="card-title">The report lending asset</h4> --}}
                                    <h4 class="card-title">{!! $it->file_name !!}</h4>
                                    <p>Download the file before you hand over bast to the team support </p>
                                    <p>{!! $it->files !!}</p>
                                    <div id="" class="my-3">
                                      <a href="/forDownload/{{ $it->id }}" class="btn waves-effect waves-light btn-rounded btn-outline-success">Download Document</a>
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                  @endforeach
                    {{-- <div class="col-lg-4 col-md-12">
                        <div class="card">
                                <div class="card-body text-center">
                                    <div class="icon-box">
                                      <div class="icon">
                                        <img class="mb-3" src="/dashboard/assets/images/big/file-text.svg" alt="" width="20%">
                                      </div>
                                      <h4 class="card-title">On reports repayment of assets</h4>
                                      <p>Download the file before you hand over bast to the team support</p>
                                      <div id="" class="my-3">
                                        <a href="/forDownload{{ $itadmin->files }}" class="btn waves-effect waves-light btn-rounded btn-outline-success">Download Document</a>
                                      </div>
                                    </div>
                              </div>
                        </div>
                    </div> --}}
                    {{-- <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <div class="card-body text-center">
                                <div class="icon-box">
                                  <div class="icon">
                                    <img class="mb-3" src="/dashboard/assets/images/big/file-text.svg" alt="" width="20%">
                                  </div>
                                  <h4 class="card-title">The report lending asset</h4>
                                  <p>Download the file before you hand over bast to the team support</p>
                                  <div id="" class="my-3">
                                    <a href="/register" class="btn waves-effect waves-light btn-rounded btn-outline-success">Download Document</a>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-lg-12 text-center">
                        <a href="/" class=" btn waves-effect waves-light btn-rounded btn-danger">Back to Home</a>
                    </div>
                </div>
                
                <!-- *************************************************************** -->
                <!-- End Sales Charts Section -->
                <!-- *************************************************************** -->

        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
@endsection
        