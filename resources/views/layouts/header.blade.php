<!-- ======= Header ======= -->
<header id="header" class="fixed-top header-transparent">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="/"><img src="/assets/img/clients/cls.png" alt=""></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="/">Home</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#team">Team</a></li>
          <li><a href="#contact">Contact</a></li>
          @auth
          <li class="drop-down"><a href="#">Welcome back, {{ auth()->user()->name }}</a>
              <ul>
                <li><a href="/admin">My Dashboard</a></li>
                <li>
                  <form action="/logout" method="post">
                    @csrf
                    <button type="submit" class="ml-3 button1">logout</button>
                  </form>
                </li>
              </ul>
            </li>
            @else
            <li><a href="/login">Login</a></li>
            @endauth          
         
        </ul>
      </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->
