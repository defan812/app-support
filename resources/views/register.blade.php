@extends('auth.main')    

@section('container')
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative"
            style="background:url(/dashboard/assets/images/big/auth-bg.jpg) no-repeat center center;">
            <div class="auth-box row text-center">
                {{-- <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url(/dashboard/assets/images/big/3.jpg);"> --}}
                </div>
                <div class="col-lg-5 col-md-7 my-5 bg-white">
                    <div class="p-3">
                        {{-- <img src="/dashboard/assets/images/big/icon.png" alt="wrapkit"> --}}
                        <h2 class="mt-3 text-center">Register Karyawan</h2>
                        <small>Will be sent by email to hrd , use to register workers in that email app-support PT CLS System</small>
                        <form action="/register" method="post" class="mt-4">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                                        @error('name')
                                           <div class="invalid-feedback">
                                            {{ $message }}
                                           </div>  
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                        @error('type_identity')
                                           <div class="invalid-feedback">
                                            {{ $message }}
                                           </div>  
                                        @enderror
                                        <select class="form-control @error('type_identity') is-invalid @enderror" name="type_identity" id="type_identity" value="{{ old('type_identity') }}" required>
                                            <option value="">Type of Identity</option>
                                            <option value=""></option>
                                            <option value="KTP">KTP</option>
                                            <option value="Paspor">Paspor</option>
                                            <option value="SIM">SIM</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('no_identity') is-invalid @enderror" name="no_identity" id="no_identity" type="text" placeholder="No Identity" value="{{ old('no_identity') }}" required >
                                        @error('no_identity')
                                        <div class="invalid-feedback">
                                         {{ $message }}
                                        </div>  
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                        @error('division')
                                        <div class="invalid-feedback">
                                         {{ $message }}
                                        </div>  
                                        @enderror
                                        <select class="form-control @error('division') is-invalid @enderror" name="division" id="division" value="{{ old('division') }}" required>
                                            <option value="">Division</option>
                                            <option value=""></option>
                                            <option value="Direction">Direction</option>
                                            <option value="HRD">HRD</option>
                                            <option value="Legal">Legal</option>
                                            <option value="Finance">Finance</option>
                                            <option value="Marketing">Marketing</option>
                                            <option value="Purchasing">Purchasing</option>
                                            <option value="IT">IT</option>
                                            <option value="Marcom">Marcom</option>
                                            <option value="Warehouse">Warehouse</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('position') is-invalid @enderror" name="position" id="position" type="text" placeholder="Position" value="{{ old('position') }}" required>
                                        @error('position')
                                        <div class="invalid-feedback">
                                         {{ $message }}
                                        </div>  
                                        @enderror
                                    </div>
                                </div>
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('telp') is-invalid @enderror" name="telp" id="telp" type="text" placeholder="No Handphone" value="{{ old('telp') }}" required>
                                        @error('telp')
                                        <div class="invalid-feedback">
                                         {{ $message }}
                                        </div>  
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('address') is-invalid @enderror" name="address" id="address" type="text" placeholder="Address" value="{{ old('address') }}" required>
                                        @error('address')
                                           <div class="invalid-feedback">
                                            {{ $message }}
                                           </div>  
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('username') is-invalid @enderror" name="username" id="username" type="text" placeholder="Username" value="{{ old('username') }}" required>
                                        @error('username')
                                           <div class="invalid-feedback">
                                            {{ $message }}
                                           </div>  
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required>
                                        @error('email')
                                        <div class="invalid-feedback">
                                         {{ $message }}
                                        </div>  
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="password">
                                        @error('password')
                                        <div class="invalid-feedback">
                                         {{ $message }}
                                        </div>  
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control" type="hidden" name="type_user" id="type_user" placeholder="" value="Karyawan">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 text-center">
                                <button type="submit" class="btn btn-block btn-dark">Sign Up</button>
                            </div>

                            <div class="col-lg-12 text-center mt-3" >
                                Already have an account? <a href="/login" class="text-danger">Sign In</a>
                            </div>

                            <div class="col-lg-12 text-center mb-3">
                                <a href="/" class="text-danger">Back to Home</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
@endsection