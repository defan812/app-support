@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                {{-- header --}}
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">
                                    Header 
                                    <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-primary border-0 mx-2" ><span data-feather="eye"></span> Header</button>
                                </h4> 
                                <hr>
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                 
                                {{-- start colapse to edit data --}}
                                <div class="collapse mt-3" id="collapseExample">
                                    <div class="card card-body">
                                        <h4 class="card-title my-2">Table Header</h4>
                                        <hr>

                                        {{-- table header --}}

                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Division</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>

                                        {{-- end table header --}}
                                        <hr>
                                        {{-- form header --}}
                                        <h4 class="card-title my-2">
                                            Form Header 
                                            <button type="button" data-toggle="collapse" data-target="#addHeader" class="btn btn-rounded btn-sm btn-primary border-0 mx-2" ><span data-feather="plus"></span> Data</button>
                                            <button type="button" data-toggle="collapse" data-target="#editHeader" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Data</button>
                                        </h4>
                                        <hr>
                                        {{-- form add header --}}
                                        <div class="collapse mt-3" id="addHeader">
                                            <form action="" method="post">
                                                @method('put')
                                                @csrf
                                                <div class="modal-body">
                                                {{-- isi content input --}}
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <small id="name" class="form-text text-muted">add header</small>
                                                                <input class="form-control @error('product_name') is-invalid @enderror" name="product_name" id="product_name" type="text" placeholder="Name" value="" required>
                                                                @error('product_name')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>  
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                        {{-- end add form header --}}
                                        <hr>
                                        {{-- form edit header --}}
                                        <div class="collapse mt-3" id="editHeader">
                                            <form action="" method="post">
                                                @method('put')
                                                @csrf
                                                <div class="modal-body">
                                                {{-- isi content input --}}
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <small id="name" class="form-text text-muted">edit header</small>
                                                                <input class="form-control @error('product_name') is-invalid @enderror" name="product_name" id="product_name" type="text" placeholder="Name" value="" required>
                                                                @error('product_name')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>  
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-warning">Changes Update</button>
                                                </div>
                                            </form>
                                        </div>
                                        {{-- end edit form header --}}

                                        {{-- end form header --}}
                                    </div>
                                </div>
                                {{-- end colapse --}}
                                
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end header --}}

               {{-- banner --}}
               <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">
                                    Header 
                                    <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-primary border-0 mx-2" ><span data-feather="eye"></span> Header</button>
                                </h4> 
                                <hr>
                                @if(session()->has('success'))
                                <div class="col-lg-6">
                                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                        role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>Success - </strong> {{ session('success') }}
                                    </div>
                                </div>
                                @endif
                                @if(session()->has('delete'))
                                <div class="col-lg-6">
                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                        role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <strong>Success - </strong> {{ session('delete') }}
                                    </div>
                                </div>
                                @endif
                                
                                {{-- start colapse to edit data --}}
                                <div class="collapse mt-3" id="collapseExample">
                                    <div class="card card-body">
                                        <h4 class="card-title my-2">Table Header</h4>
                                        <hr>

                                        {{-- table header --}}

                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Division</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>

                                        {{-- end table header --}}
                                        <hr>
                                        {{-- form header --}}
                                        <h4 class="card-title my-2">
                                            Form Header 
                                            <button type="button" data-toggle="collapse" data-target="#addHeader" class="btn btn-rounded btn-sm btn-primary border-0 mx-2" ><span data-feather="plus"></span> Data</button>
                                            <button type="button" data-toggle="collapse" data-target="#editHeader" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Data</button>
                                        </h4>
                                        <hr>
                                        {{-- form add header --}}
                                        <div class="collapse mt-3" id="addHeader">
                                            <form action="" method="post">
                                                @method('put')
                                                @csrf
                                                <div class="modal-body">
                                                {{-- isi content input --}}
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <small id="name" class="form-text text-muted">add header</small>
                                                                <input class="form-control @error('product_name') is-invalid @enderror" name="product_name" id="product_name" type="text" placeholder="Name" value="" required>
                                                                @error('product_name')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>  
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                        {{-- end add form header --}}
                                        <hr>
                                        {{-- form edit header --}}
                                        <div class="collapse mt-3" id="editHeader">
                                            <form action="" method="post">
                                                @method('put')
                                                @csrf
                                                <div class="modal-body">
                                                {{-- isi content input --}}
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <small id="name" class="form-text text-muted">edit header</small>
                                                                <input class="form-control @error('product_name') is-invalid @enderror" name="product_name" id="product_name" type="text" placeholder="Name" value="" required>
                                                                @error('product_name')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>  
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                        {{-- end edit form header --}}

                                        {{-- end form header --}}
                                    </div>
                                </div>
                                {{-- end colapse --}}
                                
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end banner --}}
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection