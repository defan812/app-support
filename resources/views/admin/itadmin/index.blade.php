@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Add File</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <form action="/admin/itadmin" method="POST"  enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    {{-- isi content input --}}

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">Name</small>
                                                    <input class="form-control @error('file_name') is-invalid @enderror" name="file_name" id="file_name" type="text" placeholder="File Name" value="{{ old('filename') }}">
                                                    @error('file_name')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div> 
                                            <div class="col-lg-12">
                                                <div class="input-group">
                                                    <small id="textHelp" class="form-text text-muted">Files</small>
                                                    <input type="file" class="form-control-file  @error('files') is-invalid @enderror" name="files" id="files" >
                                                        @error('files')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                </div>
                                            </div> 
                                        </div>     
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"
                                        data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                <div class="row">
                                    <h4 class="card-title my-2">Data IT Administration</h4>
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-info mx-2 mb-2" data-toggle="modal" data-target="#myModal"><span data-feather="plus"></span> Data</button>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>File Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($itadmin as $it)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $it->file_name }}</td>
                                                <td>{{ $it->files }}</td>
                                                <td>
                                                 <a href="/admin/itadmin/{{ $it->id }}" class="badge btn-outline-info"><span data-feather="eye"></span></a>
                                                 <a href="/forDownload/{{ $it->id }}" class="badge btn-outline-info"><span data-feather="download"></span></a>
                                                 <form action="/admin/itadmin/{{ $it->id }}" method="post" class="d-inline">
                                                     @method('delete')
                                                     @csrf
                                                     <button class="badge btn-outline-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $it->file_name }} ?')"><span data-feather="trash"></span></button>
                                                 </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection