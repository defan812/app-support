@extends('admin.layouts.main')

@section('container')  
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row d-flex justfy-content-center">
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">Detail IT Administrator</h4>
                                <hr>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>File Name</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-7">{!! $itadmin->file_name !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>File</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-7">{!! $itadmin->files !!}</h5></div>
                                    </div>
                                </div>
                                
                            <hr>
                            <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Data</button>   
                            
                            <form action="/admin/itadmin/" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-rounded btn-sm btn-danger border-0" onclick="return confirm('Are you sure , Delete this data  ?')"><span data-feather="trash"></span>Data</button>
                            </form>
                            {{-- start colapse to edit data --}}
                            <div class="collapse mt-3" id="collapseExample">
                                <div class="card card-body">
                                    <h4 class="card-title my-2">Edit Data</h4>
                                    <hr>

                                    <form action="/admin/itadmin/{{ $itadmin->id }}" method="post" enctype="multipart/form-data">
                                        @method('put')
                                        @csrf
                                        <div class="modal-body">
                                         {{-- isi content input --}}
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="textHelp" class="form-text text-muted">File Name</small>
                                                        <input type="hidden" name="oldImage" value="{{ $itadmin->files }}">
                                                        <input class="form-control @error('file_name') is-invalid @enderror" name="file_name" id="file_name" type="text" placeholder="Name" value="{{ $itadmin->file_name }}" required>
                                                        @error('file_name')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="input-group">
                                                        <small id="textHelp" class="form-text text-muted">Files</small>
                                                        <input type="file" class="form-control-file  @error('files') is-invalid @enderror" name="files" id="files" >
                                                            @error('files')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>  
                                                            @enderror
                                                    </div>
                                                </div> 
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            {{-- end colapse --}}
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


@endsection