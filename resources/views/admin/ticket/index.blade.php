@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Open Ticket</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <form action="/admin/ticket" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    {{-- isi content input --}}
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <small id="name" class="form-text text-muted">No Ticket</small>
                                                <input class="form-control @error('no_ticket') is-invalid @enderror" name="no_ticket" id="no_ticket" type="text" placeholder="No Ticket" value="{{ '#CLS-' . $request }}" readonly>
                                                @error('no_ticket')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>                                        
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <small id="name" class="form-text text-muted">Name</small>
                                                <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                                                @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>                                        
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <small id="name" class="form-text text-muted">Whatapps</small>
                                                <input class="form-control @error('whatapps') is-invalid @enderror" name="whatapps" id="whatapps" type="text" placeholder="Whatapps" value="{{ old('whatapps') }}" required>
                                                @error('whatapps')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>                                        
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <small id="name" class="form-text text-muted">Complaint</small>
                                                <input class="form-control @error('problem') is-invalid @enderror" name="problem" id="problem" type="text" placeholder="Problem" value="{{ old('problem') }}" required>
                                                @error('problem')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>                                       
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <small id="textHelp" class="form-text text-muted">Description</small>
                                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" rows="3" placeholder="Text Here..." value="{{ old('description') }}" required></textarea>
                                                @error('description')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>                                        
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <small id="textHelp" class="form-text text-muted">Image</small>
                                                <input type="file" class="form-control-file  @error('image') is-invalid @enderror" name="image" id="image" >
                                                    @error('image')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                            </div>
                                        </div> 
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input class="form-control" type="hidden" name="status" id="status" placeholder="" value="Pending">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"
                                        data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                <div class="row">
                                    <h4 class="card-title my-2">Data Ticket</h4>
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-info mx-2 mb-2" data-toggle="modal" data-target="#myModal"><span data-feather="plus"></span> Ticket</button>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>No Ticket</th>
                                                <th>Name</th>
                                                <th>Whatapps</th>
                                                <th>status</th>
                                                {{-- <th>Image</th> --}}
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                                            @foreach ($ticket as $ticket)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $ticket->no_ticket }}</td>
                                                <td>{{ $ticket->name }}</td>
                                                <td>{{ $ticket->whatapps }}</td>
                                                <td>{{ $ticket->status }}</td>
                                                {{-- <td> --}}
                                                    {{-- {{ Storage::url($ticket->image->file_name) }} --}}
                                                    {{-- <img src="{{ Storage::url($ticket->image->file_name) }}" width="50%" alt=""> --}}
                                                {{-- </td> --}}
                                                <td>
                                                 <a href="/admin/ticket/{{ $ticket->id }}" class="badge btn-outline-info"><span data-feather="eye"></span></a>
                                                 <form action="/admin/ticket/{{ $ticket->id }}" method="post" class="d-inline">
                                                     @method('delete')
                                                     @csrf
                                                     <button class="badge btn-outline-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $ticket->no_ticket }} ?')"><span data-feather="trash"></span></button>
                                                 </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection