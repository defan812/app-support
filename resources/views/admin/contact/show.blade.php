@extends('admin.layouts.main')

@section('container')  
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row d-flex justfy-content-center">
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">Detail Message</h4>
                                <hr>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Name</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $contact->names !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Email</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $contact->email !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Subject</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-7">{!! $contact->subject !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Message</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-7">{!! $contact->message !!}</h5></div>
                                    </div>
                                </div>
                            <hr>
                            <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Message</button>   
                            
                            <form action="/admin/contact/{{ $contact->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-rounded btn-sm btn-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $contact->names }} ?')"><span data-feather="trash"></span>Message</button>
                            </form>
                            {{-- start colapse to edit data --}}
                            <div class="collapse mt-3" id="collapseExample">
                                <div class="card card-body">
                                    <h4 class="card-title my-2">Edit Product</h4>
                                    <hr>

                                    <form action="/admin/contact/{{ $contact->id }}" method="post">
                                        @method('put')
                                        @csrf
                                        <div class="modal-body">
                                         {{-- isi content input --}}
                                            <div class="row">
                                                
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="names" class="form-text text-muted">Name</small>
                                                        <input class="form-control @error('names') is-invalid @enderror" name="names" id="names" type="text" placeholder="Name" value="{{ old('names', $contact->names) }}" required>
                                                        @error('names')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="email" class="form-text text-muted">Email</small>
                                                        <input class="form-control @error('email') is-invalid @enderror" name="email" id="email" type="text" placeholder="Email" value="{{ old('email', $contact->email) }}" required>
                                                        @error('email')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Subject</small>
                                                        <input class="form-control @error('subject') is-invalid @enderror" name="subject" id="subject" type="text" placeholder="Subject" value="{{ old('subject', $contact->subject) }} " required>
                                                        @error('subject')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Message</small>
                                                        <textarea class="form-control @error('message') is-invalid @enderror" name="message" id="message" type="text" placeholder="message" value="" cols="30" rows="10">{{ old('message', $contact->message) }}</textarea>
                                                        @error('message')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            {{-- end colapse --}}
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


@endsection