@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Add Message</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <form action="/admin/contact" method="POST">
                                @csrf
                                <div class="modal-body">
                                    {{-- isi content input --}}
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <small id="names" class="form-text text-muted">Name</small>
                                                <input class="form-control @error('names') is-invalid @enderror" name="names" id="names" type="text" placeholder="Name" value="{{ old('names') }}" required>
                                                @error('names')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <small id="email" class="form-text text-muted">Email</small>
                                                <input class="form-control @error('email') is-invalid @enderror" name="email" id="email" type="text" placeholder="ex : example@email.com" value="{{ old('email') }}" required>
                                                @error('email')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <small id="subject" class="form-text text-muted">Subject</small>
                                                <input class="form-control @error('subject') is-invalid @enderror" name="subject" id="subject" type="text" placeholder="subject" value="{{ old('subject') }}" required>
                                                @error('subject')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <small id="message" class="form-text text-muted">Message</small>
                                                <textarea class="form-control @error('message') is-invalid @enderror" name="message" id="message" cols="30" rows="5" value="{{ old('message') }}" required></textarea>
                                                @error('message')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>                                        
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"
                                        data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                <div class="row">
                                    <h4 class="card-title my-2">Data Message</h4>
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-info mx-2 mb-2" data-toggle="modal" data-target="#myModal"><span data-feather="plus"></span> Message</button>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Subject</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($contact as $contact)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $contact->names }}</td>
                                                <td>{{ $contact->email }}</td>
                                                <td>{{ $contact->subject }}</td>
                                                <td>
                                                    <a href="/admin/contact/{{ $contact->id }}" class="badge btn-outline-info"><span data-feather="eye"></span></a>
                                                    <form action="/admin/contact/{{ $contact->id }}" method="post" class="d-inline">
                                                        @method('delete')
                                                        @csrf
                                                        <button class="badge btn-outline-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $contact->names }} ?')"><span data-feather="trash"></span></button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection