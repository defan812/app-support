@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Add Karyawan</h4>
                            <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">×</button>
                        </div>
                        <form action="/admin/karyawan" method="POST">
                            @csrf
                            <div class="modal-body">
                                {{-- isi content input --}}
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Name</small>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                                            @error('name')
                                               <div class="invalid-feedback">
                                                {{ $message }}
                                               </div>  
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                            @error('type_identity')
                                               <div class="invalid-feedback">
                                                {{ $message }}
                                               </div>  
                                            @enderror
                                            <select class="form-control @error('type_identity') is-invalid @enderror" name="type_identity" id="type_identity" value="{{ old('type_identity') }}" required>
                                                <option class="" value="">Select One</option>
                                                <option value="KTP">KTP</option>
                                                <option value="Paspor">Paspor</option>
                                                <option value="SIM">SIM</option>
                                            </select>
                                            <small id="name" class="form-text text-muted">Type Identity</small>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">No Identity</small>
                                            <input class="form-control @error('no_identity') is-invalid @enderror" name="no_identity" id="no_identity" type="text" placeholder="No Identity" value="{{ old('no_identity') }}" required >
                                            @error('no_identity')
                                            <div class="invalid-feedback">
                                             {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                            @error('division')
                                            <div class="invalid-feedback">
                                             {{ $message }}
                                            </div>  
                                            @enderror
                                            <select class="form-control @error('division') is-invalid @enderror" name="division" id="division" value="{{ old('division') }}" required>
                                                <option value="">Select One</option>
                                                @foreach ($division as $division)  
                                                    <option value="{{ $division->division }}">{{ $division->division }}</option>
                                                @endforeach
                                            </select>
                                            <small id="name" class="form-text text-muted">Division</small>
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Position</small>
                                            <input class="form-control @error('position') is-invalid @enderror" name="position" id="position" type="text" placeholder="Position" value="{{ old('position') }}" required>
                                            @error('position')
                                            <div class="invalid-feedback">
                                             {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">No Hnadphone</small>
                                            <input class="form-control @error('telp') is-invalid @enderror" name="telp" id="telp" type="text" placeholder="No Handphone" value="{{ old('telp') }}" required>
                                            @error('telp')
                                            <div class="invalid-feedback">
                                             {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Address</small>
                                            <input class="form-control @error('address') is-invalid @enderror" name="address" id="address" type="text" placeholder="Address" value="{{ old('address') }}" required>
                                            @error('address')
                                               <div class="invalid-feedback">
                                                {{ $message }}
                                               </div>  
                                            @enderror
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Username</small>
                                            <input class="form-control @error('username') is-invalid @enderror" name="username" id="username" type="text" placeholder="Username" value="{{ old('username') }}" required>
                                            @error('username')
                                               <div class="invalid-feedback">
                                                {{ $message }}
                                               </div>  
                                            @enderror
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Email</small>
                                            <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required>
                                            @error('email')
                                            <div class="invalid-feedback">
                                             {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="name" class="form-text text-muted">Password</small>
                                            <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="password">
                                            @error('password')
                                            <div class="invalid-feedback">
                                             {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input class="form-control" type="hidden" name="type_user" id="type_user" placeholder="" value="Karyawan">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light"
                                    data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                <div class="row">
                                    <h4 class="card-title  my-2">Data Karyawan</h4>
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-info mx-2 mb-2" data-toggle="modal" data-target="#myModal"><span data-feather="plus"></span> Karyawan</button>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Type User</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($User as $user)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->position }}</td>
                                                    <td>{{ $user->username }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->type_user }}</td>
                                                    <td>
                                                        <a href="/admin/karyawan/{{ $user->id }}" class="badge btn-outline-info"><span data-feather="eye"></span></a>
                                                        {{-- <a href="#" class="badge btn-outline-warning"><span data-feather="edit"></span></a> --}}
                                                        {{-- <a href="#" class="badge btn-outline-danger"><span data-feather="trash"></a> --}}
                                                        <form action="/admin/karyawan/{{$user->id}}" method="post" class="d-inline">
                                                            @method('delete')
                                                            @csrf
                                                            <button class="badge btn-outline-danger border-0" onclick="return confirm('Are you sure , Delete this data {{$user->name}} ?')"><span data-feather="trash"></span></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection