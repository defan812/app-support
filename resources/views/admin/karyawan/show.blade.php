@extends('auth.main')    

@section('container')
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative"
            style="background:url(/dashboard/assets/images/big/auth-bg.jpg) no-repeat center center;">
            <div class="auth-box row">
                {{-- <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url(/dashboard/assets/images/big/3.jpg);"> --}}
                </div>
                <div class="col-lg-5 col-md-7 bg-white my-5">
                    <div class="p-3">
                        <div class="text-center">
                            <img src="/dashboard/assets/images/users/d1.jpg" class="rounded-circle" alt="wrapkit">
                        </div>
                        <h2 class="mt-3 text-center">{{$user->name}}</h2>
                        <p class="text-center">{{$user->type_identity}} | {{$user->no_identity}}</p>
                        <p class="text-center">{{$user->email}}</p>
                                @if(session()->has('success'))
                                   <div class="col-lg-12">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center">Username</p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-center">{{$user->username}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center">Handphone</p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-center">{{$user->telp}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center">Division</p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-center">{{$user->division}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center">Position</p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-center">{{$user->position}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="text-center">Adrress</p>
                            </div>
                            <div class="col-lg-6">
                                <p class="text-center">{{$user->address}}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="col-lg-12 text-center">
                            <a class=" btn waves-effect waves-light btn-rounded btn-warning" data-toggle="collapse"  href="#collapseExample"> Edit</a>
                            <a href="/admin/karyawan" class=" btn waves-effect waves-light btn-rounded btn-danger"> Back</a>
                        </div>
                        <div class="collapse my-3" id="collapseExample">
                            <div class="card card-body">
                                <form action="/admin/karyawan/{{ $user->id }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <div class="modal-body">
                                        {{-- isi content input --}}
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">Name</small>
                                                    <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name', $user->name) }}" required>
                                                    @error('name')
                                                       <div class="invalid-feedback">
                                                        {{ $message }}
                                                       </div>  
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                                    @error('type_identity')
                                                       <div class="invalid-feedback">
                                                        {{ $message }}
                                                       </div>  
                                                    @enderror
                                                    <select class="form-control @error('type_identity') is-invalid @enderror" name="type_identity" id="type_identity" value="{{ old('type_identity', $user->type_identity) }}" required>
                                                        {{-- <option value="">Type of Identity</option> --}}
                                                        <option value="KTP">KTP</option>
                                                        <option value="Paspor">Paspor</option>
                                                        <option value="SIM">SIM</option>
                                                    </select>
                                                    <small id="name" class="form-text text-muted">Type Identity</small>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">No Identity</small>
                                                    <input class="form-control @error('no_identity') is-invalid @enderror" name="no_identity" id="no_identity" type="text" placeholder="No Identity" value="{{ old('no_identity', $user->no_identity) }}" required >
                                                    @error('no_identity')
                                                    <div class="invalid-feedback">
                                                     {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>
            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                                    @error('division')
                                                    <div class="invalid-feedback">
                                                     {{ $message }}
                                                    </div>  
                                                    @enderror
                                                    <select class="form-control @error('division') is-invalid @enderror" name="division" id="division" value="{{ old('position', $user->division) }}" required>
                                                        {{-- <option value="">Select One</option> --}}
                                                        @foreach ($division as $division)
                                                            <option value="{{ $division->division }}">{{ $division->division }}</option>
                                                        @endforeach
                                                    </select>
                                                    <small id="name" class="form-text text-muted">Division</small>
                                                </div>
                                            </div>
            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">Position</small>
                                                    <input class="form-control @error('position') is-invalid @enderror" name="position" id="position" type="text" placeholder="Position" value="{{ old('position', $user->position) }}" required>
                                                    @error('position')
                                                    <div class="invalid-feedback">
                                                     {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">No Handphone</small>
                                                    <input class="form-control @error('telp') is-invalid @enderror" name="telp" id="telp" type="text" placeholder="No Handphone" value="{{ old('telp', $user->telp) }}" required>
                                                    @error('telp')
                                                    <div class="invalid-feedback">
                                                     {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>
            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">Address</small>
                                                    <input class="form-control @error('address') is-invalid @enderror" name="address" id="address" type="text" placeholder="Address" value="{{ old('address', $user->address) }}" required>
                                                    @error('address')
                                                       <div class="invalid-feedback">
                                                        {{ $message }}
                                                       </div>  
                                                    @enderror
                                                </div>
                                            </div>
            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">Username</small>
                                                    <input class="form-control @error('username') is-invalid @enderror" name="username" id="username" type="text" placeholder="Username" value="{{ old('username', $user->username) }}" required>
                                                    @error('username')
                                                       <div class="invalid-feedback">
                                                        {{ $message }}
                                                       </div>  
                                                    @enderror
                                                </div>
                                            </div>
            
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">email</small>
                                                    <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="email" placeholder="Email" value="{{ old('email', $user->email) }}" required>
                                                    @error('email')
                                                    <div class="invalid-feedback">
                                                     {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>
            
                                            {{-- <div class="col-lg-12">
                                                <div class="form-group"> 
                                                    <small id="name" class="form-text text-muted">password</small>
                                                    <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="password" value="{{ ($user->division) }}">
                                                    @error('password')
                                                    <div class="invalid-feedback">
                                                     {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div> --}}
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input class="form-control" type="hidden" name="type_user" id="type_user" placeholder="" value="Karyawan">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-lg-12 text-center">
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
@endsection
        