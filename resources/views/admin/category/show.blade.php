@extends('admin.layouts.main')

@section('container')  
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row d-flex justfy-content-center">
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">Detail Message</h4>
                                <hr>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Code</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $category->code !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Category</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $category->category !!}</h5></div>
                                    </div>
                                </div>
                            <hr>
                            <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Category</button>   
                            
                            <form action="/admin/category/{{ $category->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-rounded btn-sm btn-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $category->category }} ?')"><span data-feather="trash"></span>Category</button>
                            </form>
                            {{-- start colapse to edit data --}}
                            <div class="collapse mt-3" id="collapseExample">
                                <div class="card card-body">
                                    <h4 class="card-title my-2">Edit Category</h4>
                                    <hr>

                                    <form action="/admin/category/{{ $category->id }}" method="post">
                                        @method('put')
                                        @csrf
                                        <div class="modal-body">
                                         {{-- isi content input --}}
                                            <div class="row">
                                                
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="code" class="form-text text-muted">Code</small>
                                                        <input class="form-control @error('code') is-invalid @enderror" name="code" id="code" type="text" placeholder="Name" value="{{ old('code', $category->code) }}" required>
                                                        @error('code')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="category" class="form-text text-muted">category</small>
                                                        <input class="form-control @error('category') is-invalid @enderror" name="category" id="category" type="text" placeholder="category" value="{{ old('category', $category->category) }}" required>
                                                        @error('category')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            {{-- end colapse --}}
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


@endsection