@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Add Facilities</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <form action="/admin/facility" method="POST">
                                @csrf
                                <div class="modal-body">
                                    {{-- isi content input --}}

                                    <div class="col-lg-12">
                                        <div class="row">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <small id="name" class="form-text text-muted">Name</small>
                                                    <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                                                    @error('name')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <small id="no_identity" class="form-text text-muted">No iIdentity</small>
                                                    <input class="form-control @error('no_identity') is-invalid @enderror" name="no_identity" id="no_identity" type="text" placeholder="No Identity" value="{{ old('no_identity') }}" required>
                                                    @error('no_identity')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>  
                                    </div> 
                                    <div class="col-lg-12">
                                        <div class="row">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <small id="division" class="form-text text-muted">Division</small>
                                                    <select class="form-control @error('division') is-invalid @enderror" name="division" id="division" value="{{ old('division') }}" required>
                                                        <option value="">Select One</option>
                                                        @foreach ($division as $division)  
                                                            <option value="{{ $division->division }}">{{ $division->division }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('division')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>
                                        
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <small id="position" class="form-text text-muted">Position</small>
                                                    <input class="form-control @error('position') is-invalid @enderror" name="position" id="position" type="text" placeholder="Position" value="{{ old('position') }}" required>
                                                    @error('position')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>        
                                    </div> 

                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <small id="whatsapp" class="form-text text-muted">Whatsapp</small>
                                                    <input class="form-control @error('whatsapp') is-invalid @enderror" name="whatsapp" id="whatsapp" type="text" placeholder="Whatsapp" value="{{ old('whatsapp') }}" required>
                                                    @error('whatsapp')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <small id="status" class="form-text text-muted">Status</small>

                                                    <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" type="text" placeholder="Status" value="{{ old('status') }}">
                                                        <option value="">Select One</option>
                                                        <option value="Borrow">Borrow</option>
                                                        <option value="Return">Return</option>
                                                    </select>
                                                    @error('status')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>  
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>
                                    </div> 
                                     

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="address" class="form-text text-muted">Address</small>
                                            <input class="form-control @error('address') is-invalid @enderror" name="address" id="address" type="text" placeholder="Address" value="{{ old('address') }}" required>
                                            @error('address')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div> 
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <small id="note" class="form-text text-muted">note</small>
                                            <input class="form-control @error('note') is-invalid @enderror" name="note" id="note" type="text" placeholder="note" value="{{ old('note') }}" required>
                                            @error('note')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>  
                                            @enderror
                                        </div>
                                    </div> 
                                    <div class="col-lg-12 d-flex justify-content-center">
                                        <div id="dynamicAddRemove">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        {{-- <input name="addMore[0][id_facility]" id="addMore[][id_facility]" type="hidden" value=""> --}}
                                                        <div class="form-group">
                                                            <small id="name_item" class="form-text text-muted">Item Name</small>
                                                            {{-- <select class="form-control @error('name_item') is-invalid @enderror" name="name_item[]" id="addMore[][name_item]" value="{{ old('name_item') }}" >
                                                                <option value="">Select One</option>
                                                                @foreach ($item as $item)  
                                                                    <option value="{{ $item->product_name }}">{{ $item->product_name }}</option>
                                                                @endforeach
                                                            </select> --}}
                                                            <input class="form-control @error('name_item') is-invalid @enderror" name="name_item[]" id="addMore[][name_item]" type="text" placeholder="Name Item" value="{{ old('name_item') }}">
                                                            @error('name_item')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>  
                                                            @enderror
                                                        </div>
                                                    </div>
    
                                                    <div class="col-lg-5">
                                                        <div class="form-group">
                                                            <small id="brand" class="form-text text-muted">Brand</small>
                                                            <input class="form-control @error('brand') is-invalid @enderror" name="brand[]" id="addMore[][brand]" type="text" placeholder="Brand" value="{{ old('brand') }}">
                                                            @error('brand')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>  
                                                            @enderror
                                                        </div>
                                                    </div> 
                                                    
                                                    <div class="col-lg-1">

                                                    </div>
    
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <small id="qty" class="form-text text-muted">Qty</small>
                                                            <input class="form-control @error('qty') is-invalid @enderror" name="qty[]" id="addMore[][qty]" type="text" placeholder="Qty" value="{{ old('qty') }}" >
                                                            @error('qty')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>  
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-7">
                                                        <div class="form-group">
                                                            <small id="sn" class="form-text text-muted">Serial Number</small>
                                                            <input class="form-control @error('sn') is-invalid @enderror" name="sn[]" id="addMore[0][sn]" type="text" placeholder="Serial Number" value="{{ old('sn') }}" >
                                                            @error('sn')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>  
                                                            @enderror
                                                        </div> 
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <div class="form-group">
                                                            <button type="button" name="add" id="dynamic-ar" class="btn btn-rounded btn-sm btn-outline-info mt-3">Add</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            

                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"
                                        data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                <div class="row">
                                    <h4 class="card-title mx-3 my-2">Data Facilites</h4>
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-info mx-2 mb-2" data-toggle="modal" data-target="#myModal"><span data-feather="plus"></span> Facilities</button>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Whatsapp</th>
                                                {{-- <th>Item Name</th>
                                                <th>Qty</th> --}}
                                                <th>Date Craeted</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($facility as $fac)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $fac->name }}</td>
                                                <td>{{ $fac->whatsapp }}</td>
                                                {{-- <td>{{ $fac->name_item }}</td>
                                                <td>{{ $fac->qty }}</td> --}}
                                                <td>{{ $fac->created_at }}</td>
                                                <td>
                                                <a href="/admin/facility/{{ $fac->id }}" class="badge btn-outline-info"><span data-feather="eye"></span></a>
                                                 <form action="/admin/facility/{{ $fac->id }}" method="post" class="d-inline">
                                                     @method('delete')
                                                     @csrf
                                                     <button class="badge btn-outline-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $fac->name }}  ?')"><span data-feather="trash"></span></button>
                                                 </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

            <script type="text/javascript">
                var i = 0;
                $("#dynamic-ar").click(function () {
                    ++i;
                    $("#dynamicAddRemove").append('\
                    <div id="form1">\
                        <div class="col-lg-12">\
                            <div class="row">\
                                <div class="col-lg-6">\
                                    <div class="form-group">\
                                        <small id="sn" class="form-text text-muted">Serial Number</small>\
                                        <input type="text" name="name_item[]" placeholder="Item Name" class="form-control" />\
                                    </div>\
                                </div>\
                                <div class="col-lg-5">\
                                    <div class="form-group">\
                                        <small id="sn" class="form-text text-muted">Serial Number</small>\
                                        <input type="text" name="brand[]" placeholder="Item Name" class="form-control" />\
                                    </div>\
                                </div>\
                                <div class="col-lg-1">\
                                    <div class="form-group">\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                        <div class="col-lg-12">\
                            <div class="row">\
                                <div class="col-lg-4">\
                                    <div class="form-group">\
                                        <small id="sn" class="form-text text-muted">Serial Number</small>\
                                        <input type="text" name="qty[]" placeholder="Item Name" class="form-control" />\
                                    </div>\
                                </div>\
                                <div class="col-lg-7">\
                                    <div class="form-group">\
                                        <small id="sn" class="form-text text-muted">Serial Number</small>\
                                        <input type="text" name="sn[]" placeholder="Item Name" class="form-control" />\
                                    </div>\
                                </div>\
                                <div class="col-lg-1">\
                                    <div class="form-group">\
                                        <button type="button" class="btn btn-rounded btn-sm btn-outline-danger remove-input-field">Del</button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>'
                    );
                });
                $(document).on('click', '.remove-input-field', function () {
                    $(this).parents('#form1').remove();
                });
            </script>
@endsection