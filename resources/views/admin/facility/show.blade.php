@extends('admin.layouts.main')

@section('container')  
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row d-flex justfy-content-center">
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">Detail Facilities</h4>
                                <hr>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Name</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>No Identity</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Division</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Position</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Address</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Whatapps</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Status</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Note</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4"></h5></div>
                                    </div>
                                </div>
                            <hr>
                            <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Ticket</button>   
                            <form action="/admin/ticket/" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-rounded btn-sm btn-danger border-0" onclick="return confirm('Are you sure , Delete this data  ?')"><span data-feather="trash"></span>Ticket</button>
                            </form>
                            {{-- start colapse to edit data --}}
                            <div class="collapse mt-3" id="collapseExample">
                                <div class="card card-body">
                                    <h4 class="card-title my-2">Edit Ticket</h4>
                                    <hr>
                                    <form action="/admin/facility/" method="POST">
                                        @method('PUT')
                                        @csrf
                                        <div class="modal-body">
                                         {{-- isi content input --}}
                                            <div class="row">
                                                {{-- <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <input class="form-control @error('no_ticket') is-invalid @enderror" name="no_ticket" id="no_ticket" type="text" placeholder="no_ticket" value="{{ old('no_ticket', $ticket->no_ticket) }}" readonly>
                                                        @error('no_ticket')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div> --}}
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Nama</small>
                                                        <input class="form-control @error('name') is-invalid @enderror" name="name" id="name" type="text" placeholder="Name" value="" >
                                                        @error('name')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Whatapps</small>
                                                        <input class="form-control @error('whatapps') is-invalid @enderror" name="whatapps" id="whatapps" type="text" placeholder="whatapps" value="" >
                                                        @error('whatapps')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Subject</small>
                                                        <input class="form-control @error('problem') is-invalid @enderror" name="problem" id="problem" type="text" placeholder="Compalint" value=" " >
                                                        @error('spec')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Description</small>
                                                        <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" cols="30" rows="10" value=""> 
                                                            {{ old('description') }}
                                                            
                                                        </textarea>
                                                            @error('description')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>  
                                                            @enderror                                                       
                                                    </div>
                                                </div>
                                             
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Status</small>
                                                        <select class="form-control @error('status') is-invalid @enderror" name="status" id="status" value="">
                                                            <option value=""></option>
                                                            <option value=""></option>
                                                            <option value="Proses">Proses</option>
                                                            <option value="Solve">Solve</option>
                                                        </select>
                                                        {{-- <input class="form-control @error('status') is-invalid @enderror" name="status" id="status" type="text" placeholder="status" value="{{ old('status', $ticket->status) }}" required> --}}
                                                        @error('status')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            {{-- end colapse --}}
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


@endsection