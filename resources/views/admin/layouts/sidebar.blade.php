 <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin') ? 'active' : '' }} " href="/admin" aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                            class="hide-menu">Dashboard</span></a></li>
                        <li class="list-divider"></li>

                        <li class="nav-small-cap"><span class="hide-menu">General</span></li>

                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/karyawan*') ? 'active' : '' }}" href="/admin/karyawan"
                                aria-expanded="false"><i data-feather="users" class="feather-icon"></i><span class="hide-menu">Karyawan</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/division*') ? 'active' : '' }}" href="/admin/division"
                                aria-expanded="false"><i data-feather="git-pull-request" class="feather-icon"></i><span class="hide-menu">Division</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/ticket*') ? 'active' : '' }}" href="/admin/ticket"
                                aria-expanded="false"><i data-feather="bookmark" class="feather-icon"></i><span class="hide-menu">Ticket</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/itadmin*') ? 'active' : '' }}" href="/admin/itadmin"
                                aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i><span class="hide-menu">IT Administration</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/contact*') ? 'active' : '' }}" href="/admin/contact"
                                aria-expanded="false"><i data-feather="mail" class="feather-icon"></i><span class="hide-menu">Contact</span></a>
                            </li>
                            <li class="list-divider"></li>
                            <li class="nav-small-cap"><span class="hide-menu">Inventory</span></li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/item*') ? 'active' : '' }}" href="/admin/item"
                                aria-expanded="false"><i data-feather="archive" class="feather-icon"></i><span class="hide-menu">Item</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/category*') ? 'active' : '' }}" href="/admin/category"
                                aria-expanded="false"><i data-feather="sliders" class="feather-icon"></i><span class="hide-menu">Category</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link {{ Request::is('admin/category*') ? 'active' : '' }}" href="/admin/facility"
                                aria-expanded="false"><i data-feather="package" class="feather-icon"></i><span class="hide-menu">Facilities</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link" href="#"
                                aria-expanded="false"><i data-feather="package" class="feather-icon"></i><span class="hide-menu">Submission</span></a>
                            </li>
                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Report</span></li>
                            <li class="sidebar-item"> <a class="sidebar-link" href="#"
                                aria-expanded="false"><i data-feather="clipboard" class="feather-icon"></i><span class="hide-menu">General</span></a>
                            </li>
                            <li class="sidebar-item"> <a class="sidebar-link" href="#"
                                aria-expanded="false"><i data-feather="clipboard" class="feather-icon"></i><span class="hide-menu">Inventory</span></a>
                            </li>

                        <li class="list-divider"></li>
                        <li class="nav-small-cap"><span class="hide-menu">Website</span></li>

                            <li class="sidebar-item"> <a class="sidebar-link{{ Request::is('admin/category*') ? 'active' : '' }}" href="/admin/webinfo"
                                    aria-expanded="false"><i data-feather="airplay" class="feather-icon"></i><span class="hide-menu">Web Infomation</span></a>
                            </li>
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->