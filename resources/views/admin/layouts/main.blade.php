<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/dashboard/assets/images/cls.ico">
    <title>App Support | PT Cls System</title>
    <!-- Custom CSS -->
    <link href="/dashboard/assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="/dashboard/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="/dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="/dashboard/dist/css/style.css" rel="stylesheet">
    {{-- icon --}}
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <!-- This page plugin CSS -->
    <link href="/dashboard/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"> --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    
</head>

<body>
    
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
    @yield('container')
    @include('admin.layouts.footer')

</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    {{-- <script src="/dashboard/assets/libs/jquery/dist/jquery.min.js"></script> --}}
    <script src="/dashboard/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="/dashboard/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <!-- apps -->
    <script src="/dashboard/dist/js/app-style-switcher.js"></script>
    <script src="/dashboard/dist/js/feather.min.js"></script>
    <script src="/dashboard/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="/dashboard/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="/dashboard/dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <script src="/dashboard/assets/extra-libs/c3/d3.min.js"></script>
    <script src="/dashboard/assets/extra-libs/c3/c3.min.js"></script>
    <script src="/dashboard/assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="/dashboard/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <script src="/dashboard/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="/dashboard/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/dashboard/dist/js/pages/dashboards/dashboard1.min.js"></script>
    <!--datatables -->
    <script src="/dashboard/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/dashboard/dist/js/pages/datatable/datatable-basic.init.js"></script>
    
    
   
</body>

</html>