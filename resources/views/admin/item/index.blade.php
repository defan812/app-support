@extends('admin.layouts.main')

@section('container')           
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Add Product</h4>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-hidden="true">×</button>
                            </div>
                            <form action="/admin/item" method="POST">
                                @csrf
                                <div class="modal-body">
                                    {{-- isi content input --}}
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input class="form-control @error('code') is-invalid @enderror" name="code" id="code" type="text" placeholder="Code" value="{{ 'CLS-IT-' . $request }}" required>
                                                @error('code')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input class="form-control @error('product_name') is-invalid @enderror" name="product_name" id="product_name" type="text" placeholder="Name" value="{{ old('product_name') }}" required>
                                                @error('product_name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                {{-- <input class="form-control" type="text" placeholder="your name"> --}}
                                                @error('division')
                                                <div class="invalid-feedback">
                                                 {{ $message }}
                                                </div>  
                                                @enderror
                                                <select class="form-control @error('category') is-invalid @enderror" name="category" id="category" value="{{ old('category') }}" required>
                                                    <option value="">Select One</option>
                                                    @foreach ($category as $category)
                                                        <option value="{{ $category->category }}">{{ $category->category }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input class="form-control @error('branch') is-invalid @enderror" name="branch" id="branch" type="text" placeholder="Branch" value="{{ old('branch') }}" required>
                                                @error('branch')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input class="form-control @error('spec') is-invalid @enderror" name="spec" id="spec" type="text" placeholder="Specification" value="{{ old('spec') }}" required>
                                                @error('spec')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <input class="form-control @error('stock') is-invalid @enderror" name="stock" id="stock" type="text" placeholder="Stock" value="{{ old('stock') }}" required>
                                                @error('stock')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>  
                                                @enderror
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-light"
                                        data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(session()->has('success'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('success') }}
                                       </div>
                                   </div>
                                @endif
                                @if(session()->has('delete'))
                                   <div class="col-lg-6">
                                       <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                           role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                               <span aria-hidden="true">&times;</span>
                                           </button>
                                           <strong>Success - </strong> {{ session('delete') }}
                                       </div>
                                   </div>
                                @endif
                                <div class="row">
                                    <h4 class="card-title my-2">Data Item</h4>
                                    <button type="button" class="btn btn-rounded btn-sm btn-outline-info mx-2 mb-2" data-toggle="modal" data-target="#myModal"><span data-feather="plus"></span> Product</button>
                                </div>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product Name</th>
                                                <th>Branch</th>
                                                <th>Stock</th>
                                                <th>Updated Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($item as $item)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $item->product_name }}</td>
                                                    <td>{{ $item->branch }}</td>
                                                    <td>{{ $item->stock }}</td>
                                                    <td>{{ $item->updated_at }}</td>
                                                    
                                                    <td>
                                                        <a href="/admin/item/{{ $item->id }}" class="badge btn-outline-info"><span data-feather="eye"></span></a>
                                                        {{-- <a href="/admin/item/{{ $item->id }}" class="badge btn-outline-warning"><span data-feather="edit"></span></a> --}}
                                                        {{-- <a href="#" class="badge btn-outline-danger"><span data-feather="trash"></a> --}}
                                                        <form action="/admin/item/{{ $item->id }}" method="post" class="d-inline">
                                                            @method('delete')
                                                            @csrf
                                                            <button class="badge btn-outline-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $item->product_name }} ?')"><span data-feather="trash"></span></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->

@endsection