@extends('admin.layouts.main')

@section('container')  
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row d-flex justfy-content-center">
                    <div class="col-10">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title my-2">Detail Item</h4>
                                <hr>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Code</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->code !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Product Name</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->product_name !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Category</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->category !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Branch</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->branch !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Specification</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->spec !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Stock</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->stock !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Date In</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->created_at !!}</h5></div>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-4"> <h5> <strong>Date Update</strong></div>
                                        <div class="col-lg-1">:</div>
                                        <div class="col-lg-4">{!! $item->updated_at !!}</h5></div>
                                    </div>
                                </div>
                            <hr>
                            <button type="button" data-toggle="collapse" data-target="#collapseExample" class="btn btn-rounded btn-sm btn-warning border-0 mx-2" ><span data-feather="edit"></span> Product</button>   
                            
                            <form action="/admin/item/{{ $item->id }}" method="post" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-rounded btn-sm btn-danger border-0" onclick="return confirm('Are you sure , Delete this data {{ $item->product_name }} ?')"><span data-feather="trash"></span>Product</button>
                            </form>
                            {{-- start colapse to edit data --}}
                            <div class="collapse mt-3" id="collapseExample">
                                <div class="card card-body">
                                    <h4 class="card-title my-2">Edit Product</h4>
                                    <hr>

                                    <form action="/admin/item/{{ $item->id }}" method="post">
                                        @method('put')
                                        @csrf
                                        <div class="modal-body">
                                         {{-- isi content input --}}
                                            <div class="row">
                                                {{-- <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <input class="form-control @error('code') is-invalid @enderror" name="code" id="code" type="text" placeholder="Code" value="{{ old('code', $item->code) }}" required>
                                                        @error('code')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div> --}}
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Product Name</small>
                                                        <input class="form-control @error('product_name') is-invalid @enderror" name="product_name" id="product_name" type="text" placeholder="Name" value="{{ old('product_name', $item->product_name) }}" required>
                                                        @error('product_name')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Category</small>
                                                        <select class="form-control @error('category') is-invalid @enderror" name="category" id="category" value="{{ old('category') }}" required>
                                                            <option value="">{!! $item->category !!}</option>
                                                            <option value=""></option>
                                                            @foreach ($category as $category)
                                                                <option value="{{ $category->category }}">{{ $category->category }}</option>
                                                            @endforeach
                                                        </select>
                                                        {{-- <input class="form-control @error('category') is-invalid @enderror" name="category" id="category" type="text" placeholder="category" value="{{ old('category', $item->category) }}" required> --}}
                                                        @error('category')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Branch</small>
                                                        <input class="form-control @error('branch') is-invalid @enderror" name="branch" id="branch" type="text" placeholder="Branch" value="{{ old('branch', $item->branch) }}" required>
                                                        @error('branch')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Specification</small>
                                                        <input class="form-control @error('spec') is-invalid @enderror" name="spec" id="spec" type="text" placeholder="Specification" value="{{ old('spec', $item->spec) }} " required>
                                                        @error('spec')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <small id="name" class="form-text text-muted">Stock</small>
                                                        <input class="form-control @error('stock') is-invalid @enderror" name="stock" id="stock" type="text" placeholder="Stock" value="{{ old('stock', $item->stock) }}" required>
                                                        @error('stock')
                                                        <div class="invalid-feedback">
                                                            {{ $message }}
                                                        </div>  
                                                        @enderror
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            {{-- end colapse --}}
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


@endsection