<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('no_identity');
            $table->string('division');
            $table->string('position');
            $table->string('address');
            $table->string('whatsapp');
            $table->string('status');
            $table->string('note');
            $table->timestamps();

            // $table->string('name_item');
            // $table->string('brand');
            // $table->string('qty');
            // $table->string('sn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility');
    }
}
