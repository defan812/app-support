<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Division;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name'          => 'Sofyan Ardi Wibowo',
            'type_identity' => 'KTP',
            'no_identity'   => '3311061009950005',
            'division'      => 'IT',
            'position'      => 'It Support',
            'telp'          => '081375142009',
            'address'       => 'Jakarta Utara',
            'username'      => 'Jakarta Utara',
            'email'         => 'fyanshu@gmail.com',
            'password'      => '$2y$10$ZLiz/W453aM/SO3hW74jL.6ly/j.ryloWdp.PfmBzRj13jEMQ2cPa',
            'type_user'     => 'Admin'
        ]);

        Category::create([
            'code'          => 'LP',
            'category'      => 'Laptop'
        ]);

        Division::create([
            'division'      => 'IT'
        ]);
    }
}
