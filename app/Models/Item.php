<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getAutoNum()
    {
        return [
            'code' => [
                'format' => function () {
                    return 'CLS-' . 'IT-' . date('Y');
                },
                'length' => 5
            ]
        ];
    }


}
