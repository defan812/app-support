<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
    use HasFactory;

    protected $fillable = ['file_name', 'content_type', 'type', 'attachable_id', 'attachable_type'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleting(function ($model) {
            Storage::delete($model->file_name);
        });
    }

    /**
     * Get the parent imageable model (user or post).
     */
    public function attachable()
    {
        return $this->morphTo();
    }
}
