<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Category;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Item $item, Category $category, Request $request)
    {   
        // $category = Category::all();
        $request = random_int( 10000, 99999);

        return view('admin.item.index',[
            'item'      => Item::all(),
            'category'  => Category::all(),
            'request'   => $request
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'code'              => 'required|max:255',
            'product_name'      => 'required|max:255',
            'category'          => 'required|max:255',
            'branch'            => 'required|max:255',
            'spec'              => 'required|max:255',
            'stock'             => 'required|max:255'
        ]);

        Item::create($validatedData);

        return redirect('/admin/item')->with('success', 'New Product has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item, Category $category)
    {
        // $category = Category::all();
        // dd($item);
        return view('admin.item.show', [

            'item' => $item,
            'category' => Category::all()

        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //  dd($request);
        // $category = Category::all();

        $validatedData = $request->validate([
            'product_name'      => 'required|max:255',
            'category'          => 'required|max:255',
            'branch'            => 'required|max:255',
            'spec'              => 'required|max:255',
            'stock'             => 'required|max:255'
        ]);
        
        Item::where('id', $item->id)
            ->update($validatedData);

        return redirect('/admin/item')->with('success', 'Product has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        // $item1 = Item::find('id');
        // $item1 = $item;
        // Item::destroy($item->id);
        
        $item->delete();

        return redirect('/admin/item')->with('delete', 'Item has been deleted');
    }
}
