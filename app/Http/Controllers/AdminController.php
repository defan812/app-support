<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Itadmin;

class AdminController extends Controller
{
    public function index(Itadmin $itadmin)
    {
        // return view('administration');
        $itadmin = Itadmin::all();
        return view('administration',[
            'itadmin' => $itadmin
        ]);
    }
}
