<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Facility;
use App\Models\FacilityDetail;

class HomeAssetsController extends Controller
{
    public function borrow(Request $request)
    {

        $data = $request->all();
        // ddd($data);
        $facility = new Facility;
        $facility->name = $data['name'];
        $facility->no_identity = $data['no_identity'];
        $facility->division = $data['division'];
        $facility->position = $data['position'];
        $facility->address = $data['address'];
        $facility->whatsapp = $data['whatsapp'];
        $facility->status = $data['status'];
        $facility->note = $data['note'];
        $facility->save();

        for ($i = 0; $i < count($data['name_item']); $i++) {
            $facilitydetail = new FacilityDetail;
            $facilitydetail->id_facility = $facility->id;
            $facilitydetail->name_item = $data['name_item'][$i];
            $facilitydetail->brand = $data['brand'][$i];
            $facilitydetail->qty = $data['qty'][$i];
            $facilitydetail->sn = $data['sn'][$i];
            $facilitydetail->save();
        }
        
        return redirect('/')->with('success', 'New Facilities has been added');

    }
}
