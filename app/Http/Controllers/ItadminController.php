<?php

namespace App\Http\Controllers;

use App\Models\Itadmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ItadminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Itadmin $itadmin, Request $request)
    {
        
        return view('admin.itadmin.index',[
            'itadmin' => Itadmin::all(),
            'request'  => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ddd($request);
        // return $request->file('files')->store('it-files');

        $validatedData = $request->validate([
            'file_name'  => 'required|max:255',
            'files'      => 'required|file|max:1024'
        ]);

        // if($request->file('files')) {
        //     $validatedData['files'] = $request->file('files')->store('it-files');
        //     // $validatedData = public_path("it-files");
        //     // $validatedData = ['Content-Type: applycation/doc'];
        //     // $validatedData = time() ;
        // }
        
        if($request->file('files')){

            $file = $request->file('files');
            $files_name = time()."-".str_ireplace(" ","-", $file->getClientOriginalName());
            $file->move('storage/it-files', $files_name);
            $validatedData['files'] = $files_name;
        }
        
        Itadmin::create($validatedData);
        // Itadmin::create($request);

        return redirect('/admin/itadmin')->with('success', 'New Data has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Itadmin  $itadmin
     * @return \Illuminate\Http\Response
     */
    public function show(Itadmin $itadmin)
    {
        // ddd($itadmin);
        return view('admin.itadmin.show', [

            'itadmin' => $itadmin

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Itadmin  $itadmin
     * @return \Illuminate\Http\Response
     */
    public function edit(Itadmin $itadmin)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Itadmin  $itadmin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Itadmin $itadmin)
    {
        $rules = [
            'file_name' => 'required|max:255',
            'files'     => 'required|file|max:1024'
        ];

        $validatedData = $request->validate($rules);

        // ddd($request);
        if($request->file('files')) {
            if($request->oldImage) {
                File::delete('storage/it-files/'.$itadmin->files);
            }
                $file = $request->file('files');
                $files_name = time()."-".str_ireplace(" ","-", $file->getClientOriginalName());
                $file->move('storage/it-files', $files_name);
                $validatedData['files'] = $files_name;
            
        }

        Itadmin::where('id', $itadmin->id)
                 ->update($validatedData);

        return redirect('/admin/itadmin')->with('success', 'Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Itadmin  $itadmin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Itadmin $itadmin, Request $request)
    {   

        File::delete('storage/it-files/'.$itadmin->files);
        $itadmin->delete();

        return redirect('/admin/itadmin')->with('delete', 'Item has been deleted');
    }

    public function forDownload($id)
    {
        // ddd($itadmin);
        $findFiles = Itadmin::where('id', $id)->first();
        // ddd($findFiles);
    	$request = Storage::path('it-files/'.$findFiles->files);
        
        return response()->download($request);

    }
}

