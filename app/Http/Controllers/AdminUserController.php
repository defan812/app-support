<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.karyawan.index', [
            'User'      => User::all(),
            'division'  => Division::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'              => 'required|max:255',
            'type_identity'     => 'required|max:255',
            'no_identity'       => 'required|max:255',
            'division'          => 'required|max:255',
            'position'          => 'required|max:255',
            'telp'              => 'required|max:255',
            'address'           => 'required|max:255',
            'username'          => ['required', 'min:3', 'max:255', 'unique:users'],
            'email'             => 'required|email:dns|unique:users',
            'password'          => 'required|min:5|max:255',
            'type_user'         => 'required|max:255'
        ]);

        $validatedData['password'] =Hash::make($validatedData['password']);

        User::create($validatedData);

        // $request->session()->flash('success', 'Registration successful! Please Login');

        return redirect('/admin/karyawan')->with('success', 'New karyawan has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $karyawan, Division $division)
    {
        // return $user = User::find();
        return view('admin.karyawan.show', [

            'user' => $karyawan,
            'division'  => Division::all(),

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $karyawan)
    {
        $rules = [
            'name'              => 'required|max:255',
            'type_identity'     => 'required|max:255',
            'no_identity'       => 'required|max:255',
            'division'          => 'required|max:255',
            'position'          => 'required|max:255',
            'telp'              => 'required|max:255',
            'address'           => 'required|max:255',
            // 'username'          => ['required', 'min:3', 'max:255', 'unique:users'],
            // 'email'             => 'required|email:dns|unique:users',
            // 'password'          => 'required|min:5|max:255',
            'type_user'         => 'required|max:255'
        ];

       if($request->username != $karyawan->username) {
           $rules['username'] = 'required|min:3|max:255|unique:users';
           
       }elseif($request->email != $karyawan->email){
            $rules['email'] = 'required|email:dns|unique:users';
       }

       $validatedData = $request->validate($rules);

        User::where('id', $karyawan->id)
            ->update($validatedData);

        return redirect('/admin/karyawan/')->with('success', 'Karyawan has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $karyawan)
    {
        $karyawan->delete();
        
        return redirect('/admin/karyawan')->with('delete', 'Karyawan has been deleted');
    }

    // public function checkSlug(Request $request)
    // {
    //     $slug = SlugService::createSlug(Post::class, 'slug', $request->title);
    //     return response()->json(['slug'=> $slug]);
    // }
}
