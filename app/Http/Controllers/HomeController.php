<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Home;
use App\Http\Requests\TicketRequest;
use App\Models\Division;
use App\Models\Ticket;
use App\Models\Item;



class HomeController extends Controller
{
    public function index(Request $request)
    {
        $request = random_int( 10000000, 99999999);
        return view('.home',[
            'request'   => $request
        ]);

        // return view('home');
    }

    public function borrow(Request $request)
    {
        

        return view('borrow',[
            
            'division'  => Division::all(),
            'item'      => Item::all(),
            'ticket'    => Ticket::all(),
            'request'   => $request
        ]);
    }

    public function ticket(TicketRequest $request)
    {
        $ticket = Ticket::create($request->validated());
       
       if($request->file('image')) {
            $file   = $request->file('image');
            $upload = $file->store('ticket-images');

            $ticket->image()->create([
                'file_name'=>$upload, 
                'content_type'=>'image/'. $file->getClientOriginalExtension() , 
                'type'=>'photo' 
            ]);
        } 

        return redirect('/')->with('success', 'New Tikcet has been added');
    }

}
