<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class HomeMessageController extends Controller
{
    public function message(Request $request) 
    {
        $validatedData = $request->validate([
            'names'          => 'required|max:255',
            'email'         => 'required|max:255',
            'subject'       => 'required|max:255',
            'message'       => 'required|max:255'
        ]);

        Contact::create($validatedData);

        return redirect('/')->with('success', 'New Message has been added');
    }
}
