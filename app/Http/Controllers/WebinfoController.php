<?php

namespace App\Http\Controllers;

use App\Models\Webinfo;
use Illuminate\Http\Request;

class WebinfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.webinfo.index',[
            // 'webinfo' => Webinfo::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Webinfo  $webinfo
     * @return \Illuminate\Http\Response
     */
    public function show(Webinfo $webinfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Webinfo  $webinfo
     * @return \Illuminate\Http\Response
     */
    public function edit(Webinfo $webinfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Webinfo  $webinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Webinfo $webinfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Webinfo  $webinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Webinfo $webinfo)
    {
        //
    }
}
