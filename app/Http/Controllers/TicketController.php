<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use App\Http\Requests\TicketRequest;
// use Illuminate\Support\Arr;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request = random_int( 10000000, 99999999);

        return view('admin.ticket.index', [
            'ticket'    => Ticket::all(),
            'request'   => $request
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request)
    {

       $ticket = Ticket::create($request->validated());
       
       if($request->file('image')) {
            $file   = $request->file('image');
            $upload = $file->store('ticket-images');

            $ticket->image()->create([
                'file_name'=>$upload, 
                'content_type'=>'image/'. $file->getClientOriginalExtension() , 
                'type'=>'photo' 
            ]);
        } 
      



        return redirect('/admin/ticket')->with('success', 'New Tikcet has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        return view('admin.ticket.show', [

            'ticket' => $ticket,
            'category' => Ticket::all()

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $rules = [
            'name'          => 'required|max:255',
            'whatapps'      => 'required|max:255',
            'problem'       => 'required|max:255',
            'description'   => 'required|max:255',
            'status'        => 'required|max:255' 
        ];

        $validatedData = $request->validate($rules);
        
        Ticket::where('id', $ticket->id)
                ->update($validatedData);

        return redirect('/admin/ticket')->with('success', 'Ticket has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return redirect('/admin/ticket')->with('delete', 'Item has been deleted');
    }
}
