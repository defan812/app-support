<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\Division;
use App\Models\Item;
use App\Models\FacilityDetail;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Facility $facility, Division $division, Item $item, Request $request)
    {

        $data = DB::table('facilities')
        ->join('facility_details','facility_details.id_facility', '=', 'facilities.id' )
        ->get();

        return view('admin.facility.index',[
            
            'facility'  => Facility::all(),
            'division'  => Division::all(),
            'item'      => Item::all(),
            'request'   => $request,
            'data'      => $data
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        // ddd($data);
        $facility = new Facility;
        $facility->name = $data['name'];
        $facility->no_identity = $data['no_identity'];
        $facility->division = $data['division'];
        $facility->position = $data['position'];
        $facility->address = $data['address'];
        $facility->whatsapp = $data['whatsapp'];
        $facility->status = $data['status'];
        $facility->note = $data['note'];
        $facility->save();

        for ($i = 0; $i < count($data['name_item']); $i++) {
            $facilitydetail = new FacilityDetail;
            $facilitydetail->id_facility = $facility->id;
            $facilitydetail->name_item = $data['name_item'][$i];
            $facilitydetail->brand = $data['brand'][$i];
            $facilitydetail->qty = $data['qty'][$i];
            $facilitydetail->sn = $data['sn'][$i];
            $facilitydetail->save();
        }
        
        return redirect('/admin/facility')->with('success', 'New Facilities has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility, Division $division, Item $item, Request $request)
    {
        $data = DB::table('facilities')
        ->join('facility_details','facility_details.id_facility', '=', 'facilities.id' )
        ->get();

        return view('admin.facility.show', [
            'facility'  => Facility::all(),
            'division'  => Division::all(),
            'item'      => Item::all(),
            'request'   => $request,
            'data'      => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility, FacilityDetail $facilitydetail)
    {
        $data = DB::table('facilities')
        ->join('facility_details','facility_details.id_facility', '=', 'facilities.id' )
        ->first();

        $data->delete();

        return redirect('/admin/facility')->with('delete', 'Facilities has been deleted');
    }
}
