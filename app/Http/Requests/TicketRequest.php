<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     // return auth()->check();
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_ticket'         => 'required|max:255',
            'name'              => 'required|max:255',
            'whatapps'          => 'required|max:255',
            'problem'           => 'required|max:255',
            'description'       => 'required',
            'image'             => 'image|file|max:2048',
            'status'            => 'required|max:255'
        ];
    }
}
